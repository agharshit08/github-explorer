# Github Explorer

Live: [Check out the deployed app here.](https://coupler-4cb82.web.app/)


Features in the app:

- Find the N most popular repositories of user or organization based on number of forks.
- List the top M contributors and their number of commits.
- Get org/user details like location, date of account creation, avatar, github profile, total number of repos, followers, and following.
- Get repo details like repo link, stars, forks, watcher, and contributors list.
- Nice and clean UI to search and view the results.

Tech Stack: HTML, Bootstrap, and JavaScript.

Screenshot: Scroll down to see some test cases.

Steps to run locally:

- Clone the repository.
- Open public/index.html file in the browser.

![Github Explorer](test-case.png "Github Explorer")
![Github Explorer](test-case-2.png "Github Explorer")
