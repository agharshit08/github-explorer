// To handle all the UI changes i.e to add and remove profile, repos, and loader.
class Display {
  constructor() {
    this.profile = document.getElementById('profile');
    this.loader = document.getElementById('loader');
  }

  // Show basic profile details.
  displayProfile(profile) {
    this.profile.innerHTML = `
      <div class="card card-body mb-3">
        <div class="row">
          <div class="col-md-3">
            <img class="img-fluid mb-2" src="${profile.avatar_url}">
            <a href="${profile.html_url}" target="_blank" class="btn btn-primary btn-block mb-4">View Profile</a>
          </div>
          <div class="col-md-9">
            <span class="badge badge-primary">Public Repos: ${profile.public_repos}</span>
            <span class="badge badge-secondary">Public Gists: ${profile.public_gists}</span>
            <span class="badge badge-success">Followers: ${profile.followers}</span>
            <span class="badge badge-info">Following: ${profile.following}</span>
            <br><br>
            <ul class="list-group">
              <li class="list-group-item">Website/Blog: ${profile.blog}</li>
              <li class="list-group-item">Location: ${profile.location}</li>
              <li class="list-group-item">Member Since: ${profile.created_at}</li>
            </ul>
          </div>
        </div>
      </div>
      <h3 class="page-heading mb-3">Latest Repos</h3>
      <div id="repos"></div>
    `;
  }

  // Show repos requested by user.
  showRepos(repos) {
    let output = '';

    // Add HTML of every repo in output.
    repos.forEach((repo) => {

      let contributors = '';
      let numberOfContributors = 0;

      // Add HTML of every contributor in repo output.
      repo.contributors.forEach((contributor) => {
        numberOfContributors++;
        contributors += `<span class="m-1 badge badge-secondary">${contributor.login}: ${contributor.contributions}</span>`;
      });

      output += `
        <div class="card card-body mb-2">
          <div class="row">
            <div class="col-md-6">
              <a href="${repo.repoDetails.html_url}" target="_blank">${repo.repoDetails.name}</a>
            </div>
            <div class="col-md-6">
            <span class="badge badge-primary">Stars: ${repo.repoDetails.stargazers_count}</span>
            <span class="badge badge-success">Forks: ${repo.repoDetails.forks_count}</span>
            </div>
          </div>
          <br>
          <p class="lead"> Contributors: ${numberOfContributors > 0 ? numberOfContributors : 'This repository has no contributors.'} </p>
          <div class="row">
            <div class="col-md-9">
              ${contributors}
            </div>
          </div>
        </div>
      `;
    });
    
    // Output repos on web page.
    document.getElementById('repos').innerHTML = output;
  }

  // Display simple loader when results are fetching.
  showLoader(){
    const loaderHtml = `<h2> Loading... </h2>`;
    this.profile.innerHTML = '';
    this.loader.innerHTML = loaderHtml;
  }

  // Clear simple loader when results are fetched.
  clearLoader(){
    this.loader.innerHTML = '';
  }
}