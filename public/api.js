// Handle all network requests.
class API {
  constructor() {
    this.access_token = '66b20563cc7f2ae74ae0e29a980bc208ec815d40'; // Used to surpass requests limit.
    this.repos_sort = 'forks'; // sorting repos based on forks.
  }

  async getGithubProfile(username, repoCount, comCount) {

    try {

      // Get profile information.
      const profileResponse = await fetch(`https://api.github.com/users/${username}?access_token=${this.access_token}`);

      // Get repos based on number of count entered by user and top N repos based on forks.
      const reposResponse = await fetch(`https://api.github.com/search/repositories?q=user:${username}&sort=${this.repos_sort}&per_page=${repoCount}&access_token=${this.access_token}`);

      const profile = await profileResponse.json();
      const reposListData = await reposResponse.json();
      const reposList = reposListData.items;

      // Final list to be returned with all information of repos.
      const repos = [];

      // Find contributors for each repo and their commit counts.
      await Promise.all(reposList.map(async repo => {

        // Get contributors of a particular repo.
        const contributorsResponse = await fetch(`https://api.github.com/repos/${username}/${repo.name}/contributors?per_page=${comCount}&access_token=${this.access_token}`);

        // Sometimes repo information is not updated immediately so if empty repo is found then we get 204 status code.
        if (contributorsResponse.status == 204) {
          repos.push(new Repository(repo, []));
          return;
        }
        const contributors = await contributorsResponse.json();
        repos.push(new Repository(repo, contributors));
      }));

      // sort repos in descending order.
      repos.sort((r1, r2) => {
        if (r1.repoDetails.forks_count > r2.repoDetails.forks_count) {
          return -1;
        }
        if (r1.repoDetails.forks_count < r2.repoDetails.forks_count) {
          return 1;
        }
        return 0;
      });

      return {
        profile,
        repos
      }

    } catch (error) {
      // Show alert that error occured.
      alert('Unexpected error occured.' + error);
      console.log('Error occured in api.js');
      console.log(error);
    }
  }
}