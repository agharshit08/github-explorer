// Data model for repo.
class Repository{
  constructor(repoDetails, contributors){
    this.repoDetails = repoDetails;
    this.contributors = contributors;
  }
}