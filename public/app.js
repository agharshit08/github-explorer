// Init api and display.
const api = new API();
const display = new Display();

// Add functionality to submit button.
const submitButton = document.getElementById('submituser');

const submitUser = async () => {
  try {
    // Get username from form.
    const username = document.getElementById('username').value;

    // Get repository count from form.
    const repoCount = document.getElementById('repocount').value;

    // Get committees count from form.
    const comCount = document.getElementById('comcount').value;

    // Call api if all the fields are valid otherwise show invalid alert.
    if (username !== '' && repoCount !== '' && comCount !== '') {

      // Show loader when results are fetching.
      display.showLoader();
      // Fetch profile, repos, and contributors.
      const data = (await api.getGithubProfile(username, repoCount, comCount));
      display.clearLoader();

      // If username is incorrect show alert otherwise display the data.
      if (data.profile.message === 'Not Found') {
        alert('Please enter the correct username');
      } else {
        display.displayProfile(data.profile);
        display.showRepos(data.repos);
      }
    } else {
      alert('Please fill all the fields');
    }
  } catch (error) {
    // Show alert that error occured.
    alert('Unexpected error occured.' + error);
    console.log('Error occured in app.js');
    console.log(error);
  }
};